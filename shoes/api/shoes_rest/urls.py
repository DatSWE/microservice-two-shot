from django.urls import path
from .views import show_list_shoes, show_shoes_detail

urlpatterns = [
    path("api/shoes/", show_list_shoes, name="create_shoes"),
    path("api/shoes/new/", show_list_shoes, name="list_shoes"),
    path("api/shoes/<int:id>/", show_shoes_detail, name="show_shoes_detail"),
]
