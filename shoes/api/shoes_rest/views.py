from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoes, BinVO

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "name",
        "import_href",
        "id"
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
       "name",
       "manufacturer",
       "color",
       "picture_url",
       "id",
       "bin",

    ]
    encoders = {
        "bin": BinVODetailEncoder()
    }

@require_http_methods(["GET", "POST"])
def show_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400
            )


        shoes =Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def show_shoes_detail(request, id):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoes.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
