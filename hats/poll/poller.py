import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

# Import models from hats_rest, here.
# from hats_rest.models import Something

# def get_conferences():
#     response = requests.get("http://wardrobe-api:8000//api/locations/")
#     content = json.loads(response.content)
#     for location in content["location"]:
#         LocationVO.objects.update_or_create(
#             import_href=location["href"],
#             defaults={"name": location["name"]},
#         ),


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            response = requests.get('http://wardrobe-api:8000/api/locations/')
            # content = response.json()
            content = json.loads(response.content)
            for location in content['locations']:
            # for location in content['location']:
                LocationVO.objects.update_or_create(
                    import_href =location['href'],
                    defaults={'closet_name':location['closet_name']},
                    # defaults={'name':location['name']},
                )
            print("This is printed!")

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()

#send request ward api ask for all location. Run for loop for each location create or update new location
# we have VO location. Create /update with locationVO
