from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hats,LocationVO

class LocationVoEncoder(ModelEncoder):
    model =  LocationVO
    properties = ["closet_name","import_href",'id']

    

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties =[
        'fabric',
        'style',
        'color',
        'picture_url',
        'id',
        'location'
        ]
    encoders ={'location':LocationVoEncoder()}

    
    
class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties =[
        'fabric',
        'style',
        'color',
        'picture_url',
        'id',
        'location'
        ]
    encoders ={'location':LocationVoEncoder()}




# @require_http_methods(["GET","POST"])
# def api_list_hats(request,location_vo_id=None):
#     if request.method == "GET":
#         if location_vo_id is not None:
#             hats = Hats.objects.filter(location =location_vo_id)
#         else:
#             hats = Hats.objects.all()
#         return JsonResponse(
#             {"hats":hats},
#             encoder =HatsListEncoder,
#             safe=False
#         )
    
#     else:
#         content = json.loads(request.body)
#         print({"content": content})
#         try:
#             location = LocationVO.objects.get(import_href =content['location'])
#             content['location']= location
#         except LocationVO.DoesNotExist:
#             return JsonResponse({"message":"Location Does not exist"},status=400)
        
#         hats = Hats.objects.create(**content)
#         return JsonResponse(hats,encoder=HatsListEncoder,safe=False,)
@require_http_methods(["GET","POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder =HatsListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print({"content": content})
        try:
            
            location = LocationVO.objects.get(import_href = content['location'])
            print({"location": location })
            # content['href']
            content['location'] = location
            print({"content": content })

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid location"},
                status=400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

    
    # else:
    #     content = json.loads(request.body)
    #     print({"content": content})
    #     try:
    #         location = LocationVO.objects.get(import_href =content['location'])
    #         content['location']= location
    #     except LocationVO.DoesNotExist:
    #         return JsonResponse({"message":"Location Does not exist"},status=400)
        
    #     hats = Hats.objects.create(**content)
    #     return JsonResponse(hats,encoder=HatsListEncoder,safe=False,)

@require_http_methods(["GET","DELETE"])
def api_detail_hats(request,id):
    if request.method == "GET":
        hats = Hats.objects.get(id=id)
        return JsonResponse(hats,encoder =HatsListEncoder,safe=False)

    else:
         count,_ = Hats.objects.filter(id=id).delete()
         return JsonResponse({"Deleted Successful":count > 0})
