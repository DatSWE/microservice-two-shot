from .api_views import api_list_hats, api_detail_hats
from django.urls import path

urlpatterns =[
    path("hats/",api_list_hats,name ="api_list_hats"),
    path("hats/<int:id>/",api_detail_hats,name="api_detail_hats")

]
