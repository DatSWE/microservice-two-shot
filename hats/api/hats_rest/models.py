from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100,null =True)
    
    



class Hats(models.Model):
    fabric = models.CharField(max_length=200)
    style= models.CharField(max_length=200)
    color = models.CharField(max_length =200)
    picture_url = models.URLField(max_length = 255,null =True)
    
    location = models.ForeignKey(
        LocationVO,
        related_name = 'hats',
        # related_name = 'locations',
        on_delete = models.CASCADE,
        )
    
    def __str__(self):
        return self.style
