# Wardrobify

Team:

* Mitchell Wong - Shoes
* Dat Le - Hats

## Design

## Shoes microservice
For the Models, I created the Shoe model and a BinVO model to handle the search and data of the bin model in wardrobe. I then used the models created to make Encoders for the view functions to handle the https requests for grabbing a list, creating a shoe, and deleting a shoe.

I then went into the poller.py file to poll data from the bins model and tie it into the shoes. This allows us to create shoes and put it into the corresponding binID.

Hat microservice polling from location resource at port 8090. I have created models for hat that tracks the requirment properties(Style,Name,Fabric,Color and Url). The information for location is done by polling from the Wardrobe API and used for my Hat model. I then made a location value object. 

Explain your models and integration with the wardrobe
microservice, here.
