import React, { useEffect, useState } from 'react';

function ShoesForm() {
    const [bins, setBins] = useState([])
    const [hasSignedUp, setHasSignedUp] = useState(false);
    const [formData, setFormData] = useState({
        name: '',
        manufacturer: '',
        color: '',
        picture_url: '',
        bin: '',
    })

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

    const url = 'http://localhost:8080/api/shoes/';

    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
        'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
        setFormData({
            name: '',
            manufacturer: '',
            color: '',
            picture_url: '',
            bin: '',
        });
        setHasSignedUp(true);
    }
}
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
}

let messageClasses = 'alert alert-success d-none mb-0';
let formClasses = '';
if (hasSignedUp) {
  messageClasses = 'alert alert-success mb-0';
  formClasses = 'd-none';
}

return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new shoe</h1>
        <form className={formClasses} onSubmit={handleSubmit} id="create-shoes-form">

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
            <label htmlFor="name">Name</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
            <label htmlFor="manufacturer">Manufacturer</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
            <label htmlFor="color">Color</label>
          </div>

          <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
            <label htmlFor="picture_url">Picture</label>
          </div>

          <div className="mb-3">
            <select onChange={handleFormChange} required name="bin" id="bin" className="form-select">
              <option value="">Choose a bin</option>
              {bins.map(bin => {
                return (
                  <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                )
              })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
        <div className={messageClasses} id="success-message">
            Congratulations! You're shoe is registered!
        </div>
      </div>
    </div>
  </div>
)
}

export default ShoesForm
