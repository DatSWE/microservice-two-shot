import React, { useState, useEffect } from 'react';

function HatForm(props) {
  const [add,setAdd]= useState(false);
  const [fabric, setFabric] = useState('');
  const [style, setStyle] = useState('');
  const [color, setColor] = useState('');
  const [picture_url, setPicture] = useState('');
  const [locations,setLocations] =useState([]);
  const [location,setLocation] =useState('')
  

  async function fetchLocations() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }
  // const fetchLocations = async()=>{
  //   const url = 'http://localhost:8100/api/locations/';
  //   const response = await fetch(url);

  //   if (response.ok) {
  //         const data = await response.json();
  //         setLocations(data.locations);
  //         console.log(data)
  //       }
        

  // }
  
  useEffect(() => {
    fetchLocations();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      fabric,
      style,
      color,
      picture_url,
      location,
    };
   

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      
      setFabric('');
      setStyle('');
      setColor('');
      setPicture('');
      setLocation('');
      setAdd(true);

      
      
    }

 



  }

  function handleFabricChange(event) {
    const  value  = event.target.value;
    setFabric(value);
  }

  function handleStyleChange(event) {
    const value  = event.target.value;
    setStyle(value);
  }

  function handleColorChange(event) {
    const value  = event.target.value;
    setColor(value);
  }

  function handlePictureChange(event) {
    const value  = event.target.value
    setPicture(value);
  }

  function handleLocationChange(event) {
    const value  = event.target.value
    setLocation(value);
  }

  let successMessage = 'alert alert-success d-none mb-0';
  let hatFormClass = '';
  if (add) {
    successMessage = 'alert alert-success mb-0';
    hatFormClass = 'd-none';
}



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add your Hat</h1>
          <form  className = {hatFormClass} onSubmit = {handleSubmit} id="add-hat-form">
            <div className="form-floating mb-3">
              <input onChange ={handleFabricChange}  value = {fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="name">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange ={handleStyleChange} value ={style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
              <label htmlFor="starts">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input  onChange ={handleColorChange} value ={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="ends">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="picture_url" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
          </div>
            <div className="mb-3">
              <select  onChange ={handleLocationChange} required name="location" id="location" className="form-select">
                <option>Choose a location</option>
                {locations.map(location => {
                return (
                  <option key={location.href} value={location.href}>{location.closet_name}</option>
                )
              })}
              </select>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
           <div className={successMessage} id="success-message">
              Your hat has been added!
            </div>
          </div>
      </div>
    </div>
  );
}

export default HatForm;
   
