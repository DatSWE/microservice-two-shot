import React,{useState, useEffect} from 'react';


function HatList(){
    const [hats, setHats] = useState([]);

    async function loadHats() {
        const url = 'http://localhost:8090/api/hats/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setHats(data.hats);
        } else {
            console.log(response)
        }
      }

    useEffect(() => {
        loadHats();
      }, [])

    const handleDelete = async (hatId) => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`, {
        method: "DELETE",
        });
        if (response.ok) {
        setHats(hats.filter(hat => hat.id !== hatId));
        } else {
        console.error('failed to delete shoe:', await response.text());
        }
    }

    


    return (

        <table className="table">
            <thead className="thead-dark">
                <tr>
                    <th scope="col">Color</th>
                    <th scope="col">Fabric</th>
                    <th scope="col">Style</th>
                    <th scope="col">Picture_Url</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric}</td>
                            <td>{ hat.style }</td>
                            <td><img src ={ hat.picture_url } width={90} height={90}/></td>
                            <td><button onClick={() => handleDelete(hat.id)} className="btn btn-danger">Delete</button></td>
                        </tr>
                        );
                        })}
   
            </tbody>
        </table>
    );
  }


  export default HatList;
  
