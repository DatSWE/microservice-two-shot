import React, { useState, useEffect } from "react";
import App from "./App";

function ShoeList(props) {
  const [shoes, setShoes] = useState([]);

  async function loadShoes(){
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    } else {
      console.log(response)
    }
  }

  const handleDeleteShoe = async (shoeId) => {
  const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
    method: "DELETE",
  });
  if (response.ok) {
    loadShoes()
  } else {
    console.log(response)
  }
}

  useEffect(() => {
    loadShoes();
  }, []);
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={shoe.id}>
                <td>{ shoe.name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td>
                  <img src={ shoe.picture_url } className="img-thumbnail" alt="picture"/>
                  </td>
                <td>
                  <button className="btn btn-danger" onClick={() => handleDeleteShoe(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ShoeList;
