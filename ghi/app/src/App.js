import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './Shoes_Form';
import HatForm from './Hat_Form';
import HatList from './HatList'
import ShoesList from './ShoeList';
import React, { useState, useEffect } from 'react';


function App(props) {
  const [shoes, setShoes] = useState([]);

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/');
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
    } else {
      console.error()
    }
  }

  useEffect(() => {
    getShoes();
  }, []);


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/create" element={<HatForm />} />
        
        
          <Route path="/hatslist" element={<HatList />} />
          <Route path="shoes" index element={<ShoesList shoes={props.shoes} />} />
          <Route path="shoes/new" element={<ShoesForm />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
